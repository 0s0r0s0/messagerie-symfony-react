Ajout des clés de signature pour le token

génération clé privée
openssl genrsa -out config/jwt/private.pem -aes256 4096

génération clé public
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

ne pas oublier de changer dans .env JWT_PASSPHRASE pour y signifier