<?php


namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\EventDispatcher\Event;

class PasswordHashSubscriber implements EventSubscriberInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder; // initialisation de l'encoder
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [ // enregistrement de l'intercpteur et de la méthode qu'il doit déclencher
          KernelEvents::VIEW  => ['hashPassword', EventPriorities::PRE_WRITE]
        ];
    }

    public function hashPassword(ViewEvent $event) // méthode déclancher par l'intercepteur
    {
        $user = $event->getControllerResult(); // récupération de l'enveloppe passer
        $method = $event->getRequest()->getMethod(); // récup de la méthode

        // si ce n'est pas du post et ce n'est pas un User on sort
        if(!$user instanceof User || $method !== Request::METHOD_POST){
            return;
        }
        $user->setPassword( // encodage du mot de passe
            $this->encoder->encodePassword($user, $user->getPassword())
        );
    }

}