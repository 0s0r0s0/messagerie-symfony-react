<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BasicController extends AbstractController
{

    private Const DATAS = [
        [
            "id" => 1,
            "nom" => "Mme Leblanc",
            "couleur" => "blanc"
        ],
        [
            "id" => 2,
            "nom" => "Mr Violet",
            "couleur" => "violet"
        ],
        [
            "id" => 3,
            "nom" => "Mlle Rose",
            "couleur" => "rose"
        ],
    ];

    /**
     * @Route("/liste", name="listerole", methods={"get"})
     */
    public function listRole()
    {
        return $this->json(self::DATAS);
    }


}