<?php


namespace App\Controller;


use App\Entity\Weapons;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class WeaponController extends AbstractController
{

    private function getListWeapons(){
        $repo = $this->getDoctrine()
            ->getRepository(Weapons::class);
        return $repo->findAll();
    }

    /**
     * @Route("/weapons", name="listweapons", methods={"get"})
     */
    public function weapons()
    {
//        $repo = $this->getDoctrine()
//            ->getRepository(Weapons::class);
//        $weapons =  $repo->findAll();
        return $this->json($this->getListWeapons());

    }

    /**
     * @Route("/api_addWeapon", name="ajoutArme", methods={"post"})
     */
    public function addWeapons(Request $request)
    {
        /**
         * @var Serializer
         */
        $serialize = $this->get('serializer');
        $weapon =
            $serialize->deserialize($request->getContent(),
                Weapons::class, 'json');
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($weapon);
        $manager->flush();

        return $this->json($this->getListWeapons());
    }

    /**
     * @Route("/delWeapon",name="delWeapon",methods={"post","delete"})
     */
    public function deleteWeapon(Request $request)
    {
        $id = json_decode($request->getContent());
        $repo = $this->getDoctrine()
            ->getRepository(Weapons::class);
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($repo->findOneBy(['id'=> $id->id]));
        $manager->flush();
        return $this->json($this->getListWeapons());
    }
}