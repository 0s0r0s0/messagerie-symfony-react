<?php

namespace App\DataFixtures;

use App\Entity\Personnage;
use App\Entity\Room;
use App\Entity\Weapons;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    /**
     * Fonction général d'appel des loaders de données
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadWeapons($manager);
        $this->loadRoom($manager);
        $this->loadPerso($manager);
    }

    /**
     * fonction loader de génération d'arme
     * @param ObjectManager $manager
     */
    public function loadWeapons(ObjectManager $manager)
    {
        $armes = [
            "clé anglaise",
            "chandelier",
            "revolver",
            "la corde",
            "le poignard",
            "la matraque",
        ];

        foreach ($armes as $arme){
            $weapon =  new Weapons();
            $weapon->setLabel($arme);
            $manager->persist($weapon);
        }

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    public function loadRoom(ObjectManager $manager)
    {
        $rooms = [
            ['label'=>'cuisine', 'width'=>5, "height"=>5, "doorNbr"=>2],
            ['label'=>'salon', 'width'=>4, "height"=>6, "doorNbr"=>2],
            ['label'=>'célier', 'width'=>3, "height"=>5, "doorNbr"=>1],
            ['label'=>'bibliothèque', 'width'=>6, "height"=>5, "doorNbr"=>2],
            ['label'=>'bureau', 'width'=>5, "height"=>3, "doorNbr"=>2],
            ['label'=>'veranda', 'width'=>3, "height"=>3, "doorNbr"=>1],
        ];

        foreach ($rooms as $room){
            $place =  new Room();
            $place->setLabel($room['label'])
                ->setWidth($room['width'])
                ->setHeight($room['height'])
                ->setDoorNbr($room['doorNbr']);
            $manager->persist($place);
        }
        $manager->flush();
    }

    public function loadPerso(ObjectManager $manager)
    {
        $personnages = [
            ['nom' => 'mlle Rose', 'color'=>'pink', 'description' => 'lorem ipsum'],
            ['nom' => 'Dr Olive', 'color'=> 'green', 'description' => 'lorem ipsum'],
            ['nom' => 'Col Moutarde', 'color'=> 'yellow', 'description' => 'lorem ipsum'],
            ['nom' => 'Pr Violet', 'color'=> 'purple', 'description' => 'lorem ipsum'],
            ['nom' => 'Mme Pervenche', 'color'=> 'blue', 'description' => 'lorem ipsum'],
            ['nom' => 'Mme Leblanc', 'color'=> 'white', 'description' => 'lorem ipsum'],
        ];

        foreach ($personnages as $perso){
            $char = new Personnage();
            $char->setNom($perso['nom'])
                ->setColor($perso['color'])
                ->setDescription($perso['description']);
            $manager->persist($char);
        }
        $manager->flush();
    }
}
