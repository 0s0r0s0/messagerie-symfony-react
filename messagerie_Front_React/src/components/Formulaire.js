import React from "react";

class Formulaire extends React.Component{

    state = {
        message: '',
        length: this.props.length
    }

    createMessage = () => {
        const {addMessage , pseudo, length} = this.props;
        const message = {
            pseudo: pseudo,
            message: this.state.message
        };
        addMessage(message);
        this.setState({message:'',length});
    }


    handleSubmit = (event) => {
        event.preventDefault();
        this.createMessage();
    }

    handleChange = (event) => {
        const message = event.target.value;
        const length = this.props.length - message.length;
        this.setState({message:message, length:length});
    }

    handleKeyUp = (event) => {
        if(event.key === 'enter'){
            this.createMessage();
        }
    }

    render() {
        return (
            <form className="form" onSubmit={this.handleSubmit}>
                <textarea
                    maxLength="140"
                    value={this.state.message}
                    onChange={this.handleChange}
                    onKeyUp={this.handleKeyUp}
                    required
                    />
                <div className="info">{this.state.length}</div>
                <button type="submit">Envoyer</button>
            </form>
        )
    }
}

export default Formulaire;