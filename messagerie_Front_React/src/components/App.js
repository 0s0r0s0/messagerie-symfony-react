import React from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import '../App.css';
import '../animation.css';
import Formulaire from "./Formulaire";
import Connecteur from "../connecteur/Connecteur";
import Message from "./Message";


class App extends React.Component{

    state = {
        messages: {},
        pseudo: this.props.match.params.pseudo,
        configHeader: {headers:
                {'authorization': 'Bearer ' + this.props.location.state.token}}
    }

    messageRef = React.createRef();

    isPseudo = pseudo => this.state.pseudo === pseudo;

    componentDidMount() { // methode qui se déclanche à la construction du composant
        console.log(this.state);
        Connecteur.get("/api/messages", this.state.configHeader)
            .then(res => {
                this.setState({messages: res.data['hydra:member']});
               // console.log(res.data['hydra:member']);
            });
    }

    componentDidUpdate() { // se déclanche sur le setstate
        const  ref = this.messageRef.current
        ref.scrollTop = ref.scrollHeight
    }

    addMessage = message => {
        Connecteur.post("/api/messages", message, this.state.configHeader)
            .then(res => {
                const messages = { ...this.state.messages };
                messages[res.data.id] = message;
                this.setState({messages});
            });
    }


    render() {
        const msg = Object.keys(this.state.messages).map( key =>(
            <CSSTransition
                key={key}
                timeout={200}
                className="fade"
                >
                <Message
                    isPseudo={this.isPseudo}
                    pseudo={this.state.messages[key].pseudo}
                    message={this.state.messages[key].message}
                    />
            </CSSTransition>
        ));

        return (
            <div className="box">
                <div className="messages" ref={this.messageRef}>
                    <TransitionGroup className="message">
                        {msg}
                    </TransitionGroup>
                </div>
                <Formulaire
                    length={140}
                    pseudo={this.state.pseudo}
                    addMessage={this.addMessage}
                    />
            </div>
        )
    }
}

export default App;