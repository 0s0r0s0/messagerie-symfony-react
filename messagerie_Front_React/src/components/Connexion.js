import React from "react";
import { Redirect } from "react-router-dom";
import Connecteur from "../connecteur/Connecteur";


class Connexion extends React.Component{

    state = {
        pseudo: '',
        pass: '',
        goToChat: false,
        message: '',
        token: ''
    }

    handleChange = (event) => {
        const name = event.target.name;
        const val = event.target.value;
        this.setState({[name]: val});
    }


    handleSubmit = (event) => {
        event.preventDefault();
        const login = {
            username: this.state.pseudo,
            password: this.state.pass
        }

        Connecteur.post("/api/login_check", login)
            .then(res => {
                this.setState({message: "", token: res.data.token, goToChat: true})
                // localStorage.setItem("token", res.data.token)
                // this.setState({goToChat: true})
            }).catch(err => {
                this.setState({message: "Ce ne sont pas les bond identifiants de connexion"})
            })
        // this.setState({goToChat: true})
    }

    render() {

        if(this.state.goToChat){
            return <Redirect to={
                {
                    pathname:`/pseudo/${this.state.pseudo}`, state:{token: this.state.token}
                }
            } />
        }

        return (
            <div className="connexionBox">
                <form className="connexion"
                    onSubmit={this.handleSubmit}>
                    <label className="alert">{this.state.message}</label>
                    <input
                        onChange={this.handleChange}
                        required
                        name="pseudo"
                        placeholder="pseudo"
                        value={this.state.pseudo}
                        type="text" />
                    <input
                        onChange={this.handleChange}
                        required
                        name="pass"
                        value={this.state.pass}
                        type="password" />
                    <button type="submit">GO</button>
                </form>
            </div>
        )
    }
}

export default Connexion;