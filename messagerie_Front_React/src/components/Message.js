import React from "react";

const Message = ({pseudo, message, isPseudo}) => {
        if(isPseudo(pseudo)){
            return (
                <p className="user-message">{message}</p>
            )
        } else {
            return (
                <p className="not-user-message">
                    <strong>{pseudo} : <br /></strong> {message}
                </p>
            )
        }
}

export default Message;