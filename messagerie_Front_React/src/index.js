import React from "react";
import ReactDOM from "react-dom"
import {BrowserRouter, Route, Switch} from "react-router-dom"

import "./index.css";
import "./App.css";

import App from "./components/App";
import Connexion from "./components/Connexion";
import NotFound from "./components/NotFound";

const Root = () =>{
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Connexion}/>
                <Route path="/pseudo/:pseudo" component={App}/>
                <Route component={NotFound}/>
            </Switch>
        </BrowserRouter>
    )
}


ReactDOM.render(<Root/>, document.querySelector('#root'));